"""Define the relational database schema.

This module is simply executed on import

Todo:
    * `db`, `market` and `buy` are invalid constant names. However capitalizing
    them requires a lot of corrections throughout the code.
"""

# Core
from datetime import datetime, timedelta


# 3rd Party
from pydal import DAL, Field


# local
import lib.logconfig

MARKET_DOWNLOAD_INTERVAL_MINS = 5
LOG = lib.logconfig.app_log

def roundTime(dt=None, roundTo=1):
   """Round a datetime object
   dt : datetime.datetime object, default now.
   roundTo : Closest number of minutes to round to, default 1 minute.
   """
   roundTo = roundTo * 60
   if dt == None : dt = datetime.now()
   seconds = (dt.replace(tzinfo=None) - dt.min).seconds
   rounding = (seconds+roundTo/2) // roundTo * roundTo
   return dt + timedelta(0,rounding-seconds,-dt.microsecond)


def get_recent_market_data(download_interval_mins, surge_interval_mins):
        """Get price data for the 3 time points.

        surge_interval_mins <= download_interval_mins

        SurgeTrader detects changes in coin price. To do so, it subtracts the
        price of the coin at one point in time versus another point in time.
        This function gets the price data for 2 points in time so the difference
        can be calculated.
        """
        final_datetime_rounded = roundTime(roundTo=download_interval_mins)

        latest_market_ticks = dict()

        db_result = db((db.market.timestamp_rounded == final_datetime_rounded) | 
                      (db.market.timestamp_rounded == (final_datetime_rounded + timedelta(minutes=-surge_interval_mins))) |
                      (db.market.timestamp_rounded == (final_datetime_rounded + timedelta(minutes=-2*surge_interval_mins)))
                      ).select(orderby=~db.market.timestamp)

        for row in db_result:
            if latest_market_ticks.get(row.name) is None:
                latest_market_ticks[row.name] = []
            latest_market_ticks[row.name].append(row)

        if len(latest_market_ticks) == 0:
            LOG.debug("No results returned from DB")
        else:
            LOG.debug(str(len(db_result)) + " results returned from DB")

        return(latest_market_ticks)


# create DAL connection (and create DB if it doesn't exist)

db = DAL('sqlite://storage.sqlite3')

market = db.define_table(
    'market',
    Field('name'),
    Field('ask', type='double'),
    Field('timestamp_rounded', type='datetime', default=roundTime(roundTo=MARKET_DOWNLOAD_INTERVAL_MINS)),
    Field('timestamp', type='datetime', default=datetime.now)
    )

db.executesql('CREATE INDEX IF NOT EXISTS tidx ON market (timestamp);')
db.executesql('CREATE INDEX IF NOT EXISTS m_n_idx ON market (name);')

buy = db.define_table(
    'buy',
    Field('buy_id'),
    Field('config_file'),
    Field('market'),
    Field('purchase_price', type='double'),
    Field('selling_price', type='double'),
    Field('sell_id'),
    Field('amount', type='double'),
    Field('delta_percent_gain', type='double'),
    Field('additional_take_profit', type='double'),
    Field('timestamp', type='datetime', default=datetime.now)
    )
db.executesql('CREATE INDEX IF NOT EXISTS sidx ON buy (selling_price);')

debug = db.define_table(
    'debug',
    Field('config_file'),
    Field('market'),
    Field('percent_gain', type='double'),
    Field('delta_percent_gain', type='double'),
    Field('additional_take_profit', type='double'),
    Field('rate', type='double'),
    Field('timestamp_rounded', type='datetime', default=roundTime(roundTo=MARKET_DOWNLOAD_INTERVAL_MINS)),
    Field('timestamp', type='datetime', default=datetime.now)
    )
db.executesql('CREATE INDEX IF NOT EXISTS m_n_idx ON debug (market);')
