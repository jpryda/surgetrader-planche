#!/usr/bin/env python



# core
import logging
import pprint

# pypi

# local
import lib.logconfig
from . import mybittrex
from .db import db
from .db import roundTime, get_recent_market_data
from users import users


#LOG = logging.getLogger(__name__)
LOG = lib.logconfig.app_log
SURGE_TIMEDIFF_MINS = 60
DOWNLOAD_INTERVAL_MINS = 5


def single_and_double_satoshi_scalp(price):
    # forget it - huge sell walls in these low-satoshi coins!
    return price + 2e-8


def calc_sell_price(entry, gain):

    x_percent = gain / 100.0

    # Modify x_percent here to include additional take_profit from the database

    profit_target = entry * x_percent + entry

    LOG.debug(("On an entry of {0:.8f}, TP={1:.8f} for a {2} percent gain".format(
        entry, profit_target, gain)))

    return profit_target


# Pass in exchange_2 object in addition to use as little as possible - API is in beta
def post_stop_sells(config_file, exchange, exchange_2, takeprofit_percent):
    
    rows = db((db.buy.sell_id == None) & (db.buy.config_file == config_file)).select()

    for row_to_post in rows:
        
        buyorder = exchange.get_order(row_to_post['buy_id'])
        LOG.debug("row_to_post {}".format(pprint.pformat(buyorder)))
        buyorder = buyorder['result']

        # If buy order has been filled
        if not buyorder['IsOpen']:
            profit_target = calc_sell_price(entry=row_to_post.purchase_price, gain=takeprofit_percent)
            amount_to_sell = buyorder['Quantity']

            # When first posting, post a limit order at original profit target
            new_trigger_price = row_to_post.purchase_price
            new_sell_price = profit_target

            # Require exchange object with api v2.0
            LOG.debug("exchange_2.trade_sell({}, {}, {})".format(row_to_post.market, amount_to_sell, profit_target))
            result = exchange_2.trade_sell(market=row_to_post.market, order_type='LIMIT', quantity=amount_to_sell, rate=new_sell_price,
                                         time_in_effect='GOOD_TIL_CANCELLED', condition_type='STOP_LOSS_FIXED', target=new_trigger_price)
            LOG.debug("%s" % result)

            if result['success']:
                row_to_post.update_record(selling_price=profit_target, sell_id=result['result']['uuid'])
                db.commit()

        else:
            LOG.debug("""Buy has not been filled. Cannot sell for profit until it does.
                         You may want to manually cancel this buy order.""")


# Pass in exchange_2 object in addition to use as little as possible - API is in beta
def update_trailing_stop_sells(config_file, exchange, exchange_2, stop_loss_percentage, trailing_update_interval):
    
    # We require state to be stored in market table every 5 minutes
    recent_market_db = get_recent_market_data(trailing_update_interval, trailing_update_interval)

    rows = db((db.buy.sell_id != None) & (db.buy.config_file == config_file)).select()
    
    for row_to_update in rows:
        LOG.debug("\t %s" % row_to_update)

        latest_market_data = recent_market_db[row_to_update.market]

        sellorder = exchange.get_order(row_to_update['sell_id'])
        LOG.debug("row_to_update {}".format(pprint.pformat(sellorder)))
        sellorder = sellorder['result']

        # # and not buyorder['IsOpen'] Extra Cautious buyorder check: check buy order has gone through even though only updating
        # buyorder = exchange.get_order(row_to_update['buy_id'])
        # buyorder = buyorder['result']
        
        # Sell order assumed to have gone through once before but unfilled
        # if sellorder['QuantityRemaining'] > 0:
        if sellorder['IsOpen']:
            amount_to_sell = sellorder['QuantityRemaining']

            if latest_market_data[0].ask > latest_market_data[1].ask:
                
                # N.B Bad response handled in _clearprofit
                clear_order_id(exchange, row_to_update['sell_id'])

                # Set trigger and asking price to be the same
                new_trigger_price = stop_loss_percentage * latest_market_data[0].ask
                new_sell_price = new_trigger_price

                # Require exchange object with api v2.0
                LOG.debug("exchange_2.trade_sell({}, {}, {})".format(row_to_update.market, amount_to_sell, new_sell_price))
                result = exchange_2.trade_sell(market=row_to_update.market, order_type='LIMIT', quantity=amount_to_sell, rate=new_sell_price,
                                             time_in_effect='GOOD_TIL_CANCELLED', condition_type='STOP_LOSS_FIXED', target=new_trigger_price)
                LOG.debug("%s" % result)

                if result['success']:
                    row_to_update.update_record(selling_price=new_sell_price, sell_id=result['result']['uuid'])
                    db.commit()


# def _takeprofit(exchange, percent, row, order):

#     profit_target = calc_sell_price(entry=row.purchase_price, gain=percent)

#     #amount_to_sell = order['Quantity'] - 1e-8
#     amount_to_sell = order['Quantity']
#     #amount_to_sell = row['amount']

#     LOG.debug("exchange.sell_limit({}, {}, {})".format(row.market, amount_to_sell, profit_target))
#     result = exchange.sell_limit(row.market, amount_to_sell, profit_target)
#     LOG.debug("%s" % result)

#     if result['success']:
#         row.update_record(selling_price=profit_target, sell_id=result['result']['uuid'])
#         db.commit()


# #@retry()
# def takeprofit(config_file, exchange, percent):

#     rows = db((db.buy.selling_price == None) & (db.buy.config_file == config_file)).select()
#     for row in rows:
#         LOG.debug("\t %s" % row)

#         # if row['config_file'] != config_file:
#         #     LOG.debug "my config file is {} but this one is {}. skipping".format(
#         #         config_file, row['config_file'])
#         #     continue

#         order = exchange.get_order(row['buy_id'])
#         LOG.debug("unsold row {}".format(pprint.pformat(order)))
#         order = order['result']
#         if not order['IsOpen']:  # buy order closed
#             _takeprofit(exchange, percent, row, order)
#         else:
#             LOG.debug("""Buy has not been filled. Cannot sell for profit until it does.
#                   You may want to manually cancel this buy order.""")


# def _clearprofit(exchange, row):

#     LOG.debug("Clearing Profit for {}".format(row))

#     result = exchange.cancel(row['sell_id'])

#     if result['success']:
#         LOG.debug("\t\tSuccess: {}".format(result))
#         row.update_record(selling_price=None, sell_id=None)
#         db.commit()
#     else:
#         raise Exception("Order cancel failed: {}".format(result))


def clear_order_id(exchange, sell_id):
    "Used in conjunction with `invoke clearorderid`"

    row = db((db.buy.sell_id == sell_id)).select().first()
    if not row:
        raise Exception("Could not find row with sell_id {}".format(sell_id))

    LOG.debug("Clearing Profit for {}".format(row))

    result = exchange.cancel(row['sell_id'])

    if result['success']:
        LOG.debug("\t\tSuccess: {}".format(result))
        row.update_record(selling_price=None, sell_id=None)
        db.commit()
    else:
        raise Exception("Order cancel failed: {}".format(result))


def clear_all_orders(exchange):
    "Used in conjunction with `invoke cancelsells`"
    openorders = exchange.get_open_orders()['result']
    count = 0
    for openorder in openorders:
        if openorder['OrderType'] == 'LIMIT_SELL':
            count += 1
            LOG.debug("{}: {} --->{}".format(count, openorder, openorder['OrderUuid']))
            clear_order_id(exchange, openorder['OrderUuid'])

#    rows = db((db.buy.sell_id != None) & (db.buy.config_file == config_file)).select()
#    for i, row in enumerate(rows):
#        LOG.debug("  -- Row {}".format(i))
#        clear_order_id(exchange, row)


def prep(config_file, api_version='v1.1'):

    config = users.read(config_file)
    exchange = mybittrex.make_bittrex(config, api_version)
    return config, exchange


def take_profit(config_file):

    stop_loss_percent = 0.975
    trailing_update_interval = DOWNLOAD_INTERVAL_MINS
    config, exchange = prep(config_file)
    _, exchange_2 = prep(config_file, 'v2.0')
    takeprofit_percent = float(config.get('trade', 'takeprofit'))

    LOG.debug("Placing stop-sell limit orders for {}".format(config_file))
    post_stop_sells(config_file, exchange, exchange_2, takeprofit_percent)

    LOG.debug("Updating trailing stop-sell limit orders for {}".format(config_file))
    update_trailing_stop_sells(config_file, exchange, exchange_2, stop_loss_percent, trailing_update_interval)


# def clear_profit(config_file):
#     _, exchange = prep(config_file)
#     clear_all_orders(exchange)


# {
# "success" : true,
# "message" : "",
# "result" : {
#     "AccountId" : null,
#     "OrderUuid" : "0cb4c4e4-bdc7-4e13-8c13-430e587d2cc1",
#     "Exchange" : "BTC-SHLD",
#     "Type" : "LIMIT_BUY",
#     "Quantity" : 1000.00000000,
#     "QuantityRemaining" : 1000.00000000,
#     "Limit" : 0.00000001,
#     "Reserved" : 0.00001000,
#     "ReserveRemaining" : 0.00001000,
#     "CommissionReserved" : 0.00000002,
#     "CommissionReserveRemaining" : 0.00000002,
#     "CommissionPaid" : 0.00000000,
#     "Price" : 0.00000000,
#     "PricePerUnit" : null,
#     "Opened" : "2014-07-13T07:45:46.27",
#     "Closed" : null,
#     "IsOpen" : true,
#     "Sentinel" : "6c454604-22e2-4fb4-892e-179eede20972",
#     "CancelInitiated" : false,
#     "ImmediateOrCancel" : false,
#     "IsConditional" : false,
#     "Condition" : "NONE",
#     "ConditionTarget" : null
# }}
