# core
import configparser

# 3rd party

# local
from bittrex.bittrex import Bittrex


def make_bittrex(config, api_version='v1.1'):


    b = Bittrex(config.get('api', 'key'), config.get('api', 'secret'), api_version=api_version)

    return b
