"""Perform purchases of coins based on technical analysis.

Example:

        shell> invoke buy

`src/tasks.py` has a buy task that will call main of this module.

Todo:
    * This module is perfect. Are you kidding me?

"""

# core
import collections
import json
import logging
import pprint
from datetime import datetime

# 3rd party
import argh
from tenacity import retry, retry_if_exception_type, stop_after_delay, stop_after_attempt
from supycache import supycache
from bittrex.bittrex import SELL_ORDERBOOK
from numpy import diff, exp


# local
import lib.config
import lib.logconfig
from .db import db
from .db import roundTime, get_recent_market_data
from . import mybittrex

#LOGGER = logging.getLogger(__name__)
LOG = lib.logconfig.app_log
"""TODO: move LOG.debug statements to logging"""

SURGE_TIMEDIFF_MINS = 60
DOWNLOAD_INTERVAL_MINS = 5
# MIN_ACCEL = 600
# MAX_ACCEL = 10500


SYS_INI = lib.config.System()

IGNORE_BY_IN = SYS_INI.ignore_markets_by_in
"Coins we wish to avoid"

IGNORE_BY_FIND = SYS_INI.ignore_markets_by_find
"We do not trade ETH or USDT based markets"


MAX_ORDERS_PER_MARKET = SYS_INI.max_open_trades_per_market
"""The maximum number of purchases of a coin we will have open sell orders for
is 3. Sometimes a coin will surge on the hour, but drop on the day or week.
And then surge again on the hour, while dropping on the longer time charts.
We do not want to suicide our account by continually chasing a coin with this
chart pattern. MANA did this for a long time before recovering. But we dont
need that much risk."""

MIN_PRICE = SYS_INI.min_price
"""The coin must cost 100 sats or more because any percentage markup for a
cheaper coin will not lead to a change in price."""

MIN_VOLUME = SYS_INI.min_volume
"Must have at least a certain amount of BTC in transactions over last 24 hours"

MIN_GAIN = SYS_INI.min_gain
"1-hour gain must be 4% or more"

MIN_ACCEL = SYS_INI.min_accel
MAX_ACCEL = SYS_INI.max_accel


# @retry(exceptions=json.decoder.JSONDecodeError, tries=600, delay=5)
@retry(retry=retry_if_exception_type(IOError), stop=(stop_after_delay(5) | stop_after_attempt(600)))
def number_of_open_orders_in(openorders, market):
    """Maximum number of unclosed SELL LIMIT orders for a coin.

    SurgeTrader detects hourly surges. On occasion the hourly surge is part
    of a longer downtrend, leading SurgeTrader to buy on surges that do not
    close. We do not want to keep buying false surges so we limit ourselves to
    3 open orders on any one coin.

    Args:
        exchange (int): The exchange object.
        market (str): The coin.

    Returns:
        int: The number of open orders for a particular coin.

    """
    orders = list()
    open_orders_list = openorders['result']
    if open_orders_list:
        for order in open_orders_list:
            if order['Exchange'] == market:
                orders.append(order)

    return len(orders)


def percent_gain(new, old):
    """The percentage increase from old to new.

    Returns:
        float: percentage increase [0.0,100.0]
    """
    gain = (new - old) / old
    gain *= 100
    return gain


def delta_percent_gain(new, old, older):
    # import pdb
    # pdb.set_trace()

    new_delta = (new - old)
    old_delta = (old - older)
    delta_gain = ((new_delta - old_delta) / old_delta) * 100 if old_delta != 0 else None
    # print(delta_gain)
    return delta_gain


def obtain_btc_balance(exchange):
    """Get BTC balance.

    Returns:
        dict: The account's balance of BTC.
    """
    bal = exchange.get_balance('BTC')
    return bal['result']


def available_btc(exchange):
    """Get BTC balance.

    Returns:
        float: The account's balance of BTC.
    """
    bal = obtain_btc_balance(exchange)
    avail = bal['Available']
    LOG.debug("\tAvailable btc={0}".format(avail))
    return avail


def record_buy(config_file, buy_id, mkt, rate, amount, delta_percent_gain):
    """Store the details of a coin purchase.

    Create a new record in the `buy` table.

    Returns:
        Nothing
    """
    db.buy.insert(
        config_file=config_file,
        buy_id=buy_id, market=mkt, purchase_price=rate, amount=amount, 
        delta_percent_gain=delta_percent_gain, additional_take_profit=additional_take_profit(delta_percent_gain, 3, .003))
    db.commit()


def rate_for(exchange, mkt, btc):
    "Return the rate that allows you to spend a particular amount of BTC."

    coin_amount = 0
    btc_spent = 0
    orders = exchange.get_orderbook(mkt, SELL_ORDERBOOK)
    for order in orders['result']:
        btc_spent += order['Rate'] * order['Quantity']
        if btc_spent > 1.4* btc:
            coin_amount = btc / order['Rate']
            return order['Rate'], coin_amount
    return 0


def percent2ratio(percentage):
    """Convert a percentage to a float.

    Example:
        if percentage == 5, then return 5/100.0:

    """
    return percentage / 100.0


def calculate_trade_size(user_config):
    """How much BTC to allocate to a trade.

    Given the seed deposit and the percentage of the seed to allocate to each
    trade.

    Returns:
        float : the amount of BTC to spend on trade.
    """

    holdings = user_config.trade_deposit
    trade_ratio = percent2ratio(user_config.trade_trade)

    return holdings * trade_ratio


def get_trade_size(user_config, btc):
    "Determine how much BTC to spend on a buy."

    # Do not trade if we are configured to accumulate btc
    # (presumably for withdrawing a percentage for profits)
    if btc <= user_config.trade_preserve:
        LOG.debug("BTC balance <= amount to preserve")
        return 0

    # If we have more BTC than the size of each trade, then
    # make a trade of that size
    trade_size = calculate_trade_size(user_config)
    LOG.debug("\tTrade size   ={}".format(trade_size))
    if btc >= trade_size:
        return trade_size

    # Otherwise do not trade
    return 0


def fee_adjust(btc, exchange):
    """The amount of BTC that can be spent on coins sans fees.

    For instance if you want to spend 0.03BTC per trade, but the exchange charges 0.25% per trade,
    then you can spend 0.03 -  0.03 * 0.0025 instead of 0.03
    """

    exchange_fee = 0.25 # 0.25% on Bittrex
    LOG.debug("Adjusting {} trade size to respect {}% exchange fee on {}".format(
        btc, exchange_fee, exchange))

    exchange_fee /= 100.0

    adjusted_spend = btc - btc * exchange_fee
    return adjusted_spend



def _buycoin(config_file, user_config, exchange, mkt, btc, delta_percent_gain, percent_gain):
    "Buy into market using BTC."

    # !!! DEBUG
    debug_rate, debug_amount_of_coin = rate_for(exchange, mkt, 1)
    db.debug.insert(
        config_file=config_file,
        market=mkt,
        percent_gain=percent_gain,
        delta_percent_gain=delta_percent_gain, 
        additional_take_profit=additional_take_profit(delta_percent_gain, 3, .003),
        rate=debug_rate
        )
    db.commit()
    # !!! DEBUG end

    size = get_trade_size(user_config, btc)
    if not size:
        LOG.debug("No trade size. Returning.")
        return
    else:
        size = fee_adjust(size, exchange)
    
    LOG.debug("I will trade {0} BTC.".format(size))

    rate, amount_of_coin = rate_for(exchange, mkt, size)

    LOG.debug("I get {0} units of {1} at the rate of {2:.8f} BTC per coin.".format(
        amount_of_coin, mkt, rate))


    # result = exchange.buy_limit(mkt, amount_of_coin, rate)
    # if result['success']:
    #     LOG.debug("\tBuy was a success = {}".format(result))
    #     record_buy(config_file, result['result']['uuid'], mkt, rate, amount_of_coin, delta_percent_gain)
    # else:
    #     LOG.debug("\tBuy FAILED: {}".format(result))


def buycoin(config_file, user_config, exchange, top_coins):
    "Buy top N cryptocurrencies."

    avail = available_btc(exchange)

    for market in top_coins:
        _buycoin(config_file, user_config, exchange, market[0], avail, market[5], market[1])


@supycache(cache_key='result')
def analyze_gain(exchange):
    """Find the increase in coin price.

    The market database table stores the current ask price of all coins.
    Every hour `invoke download` creates another row in this table. Then when
    `invoke buy` gets to the analyze_gain function, analyze_gain pulls the 2
    most recent rows from market and subtracts the ask prices to determine the
    1-hour price gain.

    Returns:
        list : A list of 5-tuples of this form
           (
                name,  # the market name, e.g. "BTC-NEO"
                percent_gain(row[0].ask, row[1].ask), # 1-hour price gain
                row[1].ask, # price this hour
                row[0].ask, # prince 1 hour ago
                'https://bittrex.com/Market/Index?MarketName={0}'.format(name),
            )
    """
    def should_skip(name):
        """Decide if a coin should be part of surge analysis.

        IGNORE_BY_IN filters out coins that I do not trust.
        IGNORE_BY_FIND filters out markets that are not BTC-based.
          E.g: ETH and USDT markets.
        """
        for ignorable in IGNORE_BY_IN:
            if ignorable in name:
                LOG.debug("\tIgnoring {} because {} is in({}).".format(
                    name, ignorable, IGNORE_BY_IN))
                return True

        for ignore_string in IGNORE_BY_FIND:
            if name.find(ignore_string) > -1:
                LOG.debug('\tIgnore by find: ' + name)
                return True

        return False


    markets_list = exchange.get_market_summaries()
    markets = dict()

    for row in markets_list['result']:
        markets[row['MarketName']] = row
    
    recent_market_db = get_recent_market_data(DOWNLOAD_INTERVAL_MINS, SURGE_TIMEDIFF_MINS)

    openorders = exchange.get_open_orders()

    LOG.debug("<ANALYZE_GAIN numberofmarkets={0}>".format(len(list(recent_market_db.keys()))))

    gain = list()

    for name, row in recent_market_db.items():

        LOG.debug("Analysing {}...".format(name))

        if len(row) < 3:
            LOG.debug("\t3 market entries required")
            continue

        # dl_timestamps = [x.timestamp for x in row]
        # timestamp_deltas = diff(dl_timestamps[::-1]) # Reverse list so oldest timestamp at end
        # if not (all(timestamp_deltas < datetime.timedelta(minutes = 17)) and all(timestamp_deltas > datetime.timedelta(minutes = 13))):
        #     print("\tTime intervals from db do not match CRON setting of 15 minutes")
        #     continue

        if should_skip(name):
            continue

        try:
            if markets[name]['BaseVolume'] < MIN_VOLUME:
                LOG.debug("\t{} 24hr vol < {}".format(markets[name], MIN_VOLUME))
                continue
        except KeyError:
            LOG.debug("\tKeyError locating {}".format(name))
            continue

        if number_of_open_orders_in(openorders, name) >= MAX_ORDERS_PER_MARKET:
            LOG.debug('\tToo many open orders: ' + name)
            continue

        if row[0].ask < MIN_PRICE:
            LOG.debug('\t{} costs less than {}.'.format(name, MIN_PRICE))
            continue

        gain.append(
            (
                name,
                percent_gain(row[0].ask, row[1].ask),
                row[1].ask,
                row[0].ask,
                'https://bittrex.com/Market/Index?MarketName={0}'.format(name),
                delta_percent_gain(row[0].ask, row[1].ask, row[2].ask)
            )
        )

    LOG.debug("</ANALYZE_GAIN>")

    gain = sorted(gain, key=lambda r: r[1], reverse=True)
    return gain


def topcoins(exchange, number_of_coins):
    """Find the coins with the greatest change in price.

    Calculate the gain of all BTC-based markets. A market is where
    one coin is exchanged for another, e.g: BTC-XRP.

    Markets must meet certain criteria:
        * 24-hr volume of MIN_VOLUME
        * price gain of MIN_GAIN
        * BTC-based market only
        * Not filtered out because of should_skip()
        * Cost is 125 satoshis or more

    Returns:
        list : the markets which are surging.
    """
    top = analyze_gain(exchange)

    # LOG.debug 'TOP: {}.. now filtering'.format(top[:10])
    top = [t for t in top if t[1] >= MIN_GAIN and MIN_ACCEL <= t[5] <= MAX_ACCEL]
    # LOG.debug 'TOP filtered on MIN_GAIN : {}'.format(top)

    LOG.debug("Top 5 coins filtered on %gain={} and volume={}:\n{}".format(
        MIN_GAIN,
        MIN_VOLUME,
        pprint.pformat(top[:5], indent=4)))

    return top[:number_of_coins]


def process(config_file):
    """Buy coins for every configured user of the bot."""
    user_config = lib.config.User(config_file)

    exchange = mybittrex.make_bittrex(user_config.config)

    top_coins = topcoins(exchange, user_config.trade_top)

    LOG.debug("------------------------------------------------------------")
    LOG.debug("Buying coins for: {}".format(config_file))
    buycoin(config_file, user_config, exchange, top_coins)


def additional_take_profit(x, max_additional_tp, time_constant):
    if x is None or x < 0:
        return 0
    else:
        y = max_additional_tp*(1-exp(-time_constant*x))
        return y 


def main(inis):
    """Buy coins for every configured user of the bot."""

    for config_file in inis:
        process(config_file)

if __name__ == '__main__':
    argh.dispatch_command(main)
